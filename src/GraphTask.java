import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

	/** Main method. */
	public static void main (String[] args) {
		GraphTask a = new GraphTask();
		a.run();
	}

	/** Actual main method to run examples and everything. */
	public void run() {


		Graph g = new Graph ("G");
		g.createRandomDAG (2000, 50000, 10);
		System.out.println (g);

		List<Vertex> topoSorted = g.topologicalSort();
		System.out.println();

		Vertex vi = g.first;
		for (int i = 0; i < 1999; i++) {
			vi = vi.next;
		}
		Vertex vj = g.first;
		long current = System.currentTimeMillis();
		Graph path = g.longestDirectedPath(vi, vj);

		System.out.println(path);
		System.out.println(System.currentTimeMillis() - current);
		/*
		Graph test = new Graph("Test");

		Vertex v6 = test.createVertex("v6");
		Vertex v5 = test.createVertex("v5");
		Vertex v4 = test.createVertex("v4");
		Vertex v3 = test.createVertex("v3");
		Vertex v2 = test.createVertex("v2");
		Vertex v1 = test.createVertex("v1");
		test.createArcWithWeight("a" + v6.id + "_" + v5.id, v6, v5, -2);
		test.createArcWithWeight("a" + v6.id + "_" + v4.id, v6, v4, -3);
		test.createArcWithWeight("a" + v5.id + "_" + v3.id, v5, v3, 5);
		test.createArcWithWeight("a" + v5.id + "_" + v1.id, v5, v1, -3);
		test.createArcWithWeight("a" + v5.id + "_" + v4.id, v5, v4, -9);
		test.createArcWithWeight("a" + v4.id + "_" + v3.id, v4, v3, 6);
		test.createArcWithWeight("a" + v4.id + "_" + v1.id, v4, v1, 7);
		test.createArcWithWeight("a" + v3.id + "_" + v2.id, v3, v2, -5);
		test.createArcWithWeight("a" + v2.id + "_" + v1.id, v2, v1, 0);
		test.weightBound = 10;

		System.out.println(test);

		Graph testPath = test.longestDirectedPath(v6, v1);

		System.out.println(testPath);

			/*
		Graph g = new Graph ("G");
		g.createRandomDAG (2000, 1900000, 10);
		System.out.println (g);

		List<Vertex> topoSorted = g.topologicalSort();
		System.out.println();

		Vertex v1 = g.first.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next;
		Vertex v2 = g.first.next.next.next.next.next.next.next;
		Graph path = g.longestDirectedPath(v1, v2);

		System.out.println(path);

			 */

	}


	class Vertex {

		private String id;
		private Vertex next;
		private Arc first;

		private int topoOrder = 0;
		private boolean visited = false;
		// indicates maximum path length from source vertex to this vertex
		private int maxLength;
		// string that represents sequence of arcs
		// path from source vertex to this vertex
		// used to build longest path as graph
		private String longestPath;
		//used in topological sort to indicate if vertex was processed
		private int mark = 0; // 0 - no mark,
		// 1 - temporary,
		// 2 - permanent

		Vertex (String s, Vertex v, Arc e) {
			id = s;
			next = v;
			first = e;
		}

		Vertex (String s) {this (s, null, null);}

		@Override
		public String toString() { return id; }

	}


	/** Arc represents one arrow in the graph. Two-directional edges are
	 * represented by two Arc objects (for both directions).
	 */
	class Arc {

		private String id;
		private Vertex target;
		private Arc next;
		private int weight;

		Arc (String s, Vertex v, Arc a, int w) {
			id = s;
			target = v;
			next = a;
			weight = w;
		}

		Arc (String s) {
			this (s, null, null, 0);
		}

		@Override
		public String toString() {
			return id + ":" + weight;
		}

	}

	class Graph {

		private String id;
		private Vertex first;
		private int info = 0;
		// You can add more fields, if needed
		private int size;
		private int arcAmount; // amount of edges (arcs)
		private int weightBound; // maximum and minimum (-weightBound) value of edge weight

		Graph(String s, Vertex v) {
			id = s;
			first = v;
		}

		Graph(String s) {
			this(s, null);
		}

		@Override
		public String toString() {
			String nl = System.getProperty("line.separator");
			StringBuffer sb = new StringBuffer(nl);
			sb.append(id);
			sb.append(nl);
			Vertex v = first;
			while (v != null) {
				sb.append(v.toString());
				sb.append(" -->");
				Arc a = v.first;
				while (a != null) {
					sb.append(" ");
					sb.append(a.toString());
					sb.append(" (");
					sb.append(v.toString());
					sb.append("->");
					sb.append(a.target.toString());
					sb.append(")");
					a = a.next;
				}
				sb.append(nl);
				v = v.next;
			}
			return sb.toString();
		}

		public Vertex createVertex(String vid) {
			Vertex res = new Vertex(vid);
			res.next = first;
			first = res;
			return res;
		}

		/**
		 * Generate arc between two vertices
		 * @param aid arc id
		 * @param from source vertex
		 * @param to destination vertex
		 * @param weight arc weight
		 * @return generated arc
		 */
		public Arc createArcWithWeight(String aid, Vertex from, Vertex to, int weight) {
			Arc res = new Arc(aid);
			res.next = from.first;
			from.first = res;
			res.target = to;
			res.weight = weight;
			return res;
		}


		/**
		 * Create an adjacency matrix of this graph.
		 * Side effect: corrupts topoOrder fields in the graph
		 *
		 * @return adjacency matrix
		 */
		public int[][] createAdjMatrix() {
			info = 0;
			Vertex v = first;
			while (v != null) {
				v.topoOrder = info++;
				v = v.next;
			}
			int[][] res = new int[info][info];
			v = first;
			while (v != null) {
				int i = v.topoOrder;
				Arc a = v.first;
				while (a != null) {
					int j = a.target.topoOrder;
					res[i][j]++;
					a = a.next;
				}
				v = v.next;
			}
			return res;
		}

		// My methods

		/**
		 * Create a connected directed random tree with n vertices.
		 * Each new vertex is connected to some random existing vertex.
		 *
		 * @param n number of vertices added to this graph
		 */
		public void createRandomTreeForDirectedGraph(int n) {
			if (n <= 0)
				return;
			Vertex[] varray = new Vertex[n];
			for (int i = 0; i < n; i++) {
				varray[i] = createVertex("v" + (n - i));
				if (i > 0) {
					int vnr = (int) (Math.random() * i);
					createArcWithWeight("a" + varray[vnr].toString() + "_"
									+ varray[i].toString(), varray[vnr], varray[i],
							(int) (-weightBound + Math.random() * weightBound * 2));
				}
			}
		}

		/**
		 * Create a directed  acyclic weighted random graph with n vertices and m edges.
		 * Cannot handle very big amount of arcs
		 * @param n number of vertices
		 * @param m number of edges
		 */
		public void createRandomDAG(int n, int m, int weightBound) {
			if (n <= 0)
				return;
			if (n > 2500)
				throw new IllegalArgumentException("Too many vertices: " + n);
			if (m < n - 1 || m > n * (n - 1) / 2)
				throw new IllegalArgumentException
						("Impossible number of edges: " + m);
			size = n;
			arcAmount = m;
			this.weightBound = weightBound;
			first = null;
			createRandomTreeForDirectedGraph(n);       // n-1 edges created here
			List<Vertex> sorted = topologicalSort(); // topologically ordered vertices
			int[][] connected = createAdjMatrix();
			int edgeCount = m - n + 1;  // remaining edges
			while (edgeCount > 0) {
				int i = (int) (Math.random() * n);  // random source
				if (i == 0) continue;
				int j = (int) (Math.random() * i);  // random target
				if (connected[i][j] != 0 || connected[j][i] != 0)
					continue;  // no multiple edges
				Vertex vi = sorted.get(i);
				Vertex vj = sorted.get(j);
				createArcWithWeight("a" + vi.toString() + "_" + vj.toString(),
						vi, vj, (int) (-weightBound + Math.random() * weightBound * 2));
				connected[i][j] = 1;
				edgeCount--;  // a new edge happily created
			}
		}


		/**
		 * Clear marks
		 * @param root first vertex in sequence
		 */
		private void clearMarks(Vertex root) {
			Vertex v = root;
			while (v != null) {
				v.mark = 0;
				v = v.next;
			}
		}


		/**
		 * Orders of vertices such that for every directed edge uv,
		 * vertex u comes before v in the ordering.
		 * Uses depth first search
		 * Assigns values of topological ordering to
		 * topoOrder property of every vertex
		 * For reference was used pseudocode from
		 * https://en.wikipedia.org/wiki/Topological_sorting
		 * @return topologically sorted list
		 */
		private List<Vertex> topologicalSort() {
			List<Vertex> sorted = new ArrayList<>(size);
			Vertex vCurrent = first;
			while (vCurrent != null) {
				if (vCurrent.mark == 0) {
					visit(vCurrent, sorted);
				}
				vCurrent = vCurrent.next;
			}
			for (int i = 0; i < sorted.size(); i++) {
				sorted.get(i).topoOrder = i + 1;

			}
			clearMarks(first);
			return sorted;
		}

		/**
		 * Auxiliary method for topologicalSort
		 * @param v vertex
		 * @param sorted list of ordered vertices
		 */
		private void visit(Vertex v, List<Vertex> sorted) {
			if (v.mark == 2) return;
			if (v.mark == 1) // if mark is 1 at this point it means that recursion loop occured
				throw new RuntimeException("Not DAG - cycle on " + v.id +"!!!");
			v.mark = 1;
			v.maxLength = Integer.MIN_VALUE;
			Arc a = v.first;
			while (a != null) {
				visit(a.target, sorted);
				a = a.next;
			}
			v.mark = 2;
			sorted.add(v);
		}

		/**
		 * Main method to execute to get longest directed path
		 * @param from source vertex
		 * @param to destination vertex
		 * @return longest directed path
		 */
		public Graph longestDirectedPath(Vertex from, Vertex to) {
			List<Vertex> sorted = topologicalSort();

			if (from.topoOrder < to.topoOrder)
				throw new RuntimeException("No path from " + from + " to " + to);
			if (from.topoOrder == to.topoOrder) // No path to itself
				throw new RuntimeException("Same source and destination vertex: " + from + " " + to);

			int length = Integer.MIN_VALUE;

			setMaxLenForTree(from, length, "");

			if (to.longestPath == null) {
				throw new RuntimeException("No path from " + from + " to " + to);
			}

			return buildPathFromString(to.longestPath);
		}

		/**
		 * Set max length to every vertex in a tree
		 * Also assigns path with said length
		 * @param root tree root
		 * @param path string representing sequence of arcs from source to root
		 */
		private void setMaxLenForTree(Vertex root, int length, String path) {
			if (root == null) return;

			if (root.maxLength < length || root.maxLength == Integer.MIN_VALUE) {
				root.maxLength = length;
				root.longestPath = path;
				Arc a = root.first;
				while (a != null) {
					if (length == Integer.MIN_VALUE) {
						setMaxLenForTree(a.target, a.weight,
								path + " " + a.toString());
					} else {
						setMaxLenForTree(a.target, length + a.weight,
								path + " " + a.toString());
					}
					a = a.next;
				}
			}
		}

		/**
		 * Build graph from string
		 * @param path string
		 * @return path as graph
		 */
		private Graph buildPathFromString(String path) {
			Graph result = new Graph("Longest path");
			String[] arcs = path.trim().split(" ");
			Stack<Vertex> vertices = new Stack<>();
			int totalLength = 0;
			for (int i = 0; i < arcs.length; i++) {
				String aid = arcs[i].split(":")[0];
				String arc = aid.substring(1);

				Vertex v1;
				if (i == 0) {
					v1 = new Vertex(arc.split("_")[0]);
					result.first = v1;
				} else {
					v1 = vertices.pop();
				}
				Vertex v2 = new Vertex(arc.split("_")[1]);

				int length = Integer.valueOf(arcs[i].split(":")[1]);
				totalLength += length;
				createArcWithWeight(aid, v1, v2, length);
				v2.maxLength = totalLength;

				v1.next = v2;

				vertices.push(v1);
				vertices.push(v2);
			}
			result.id += "(" + vertices.pop().maxLength + ")";

			return result;
		}
	}
} 

